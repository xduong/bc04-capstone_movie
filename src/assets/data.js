export const dataPhim = [
  {
    maPhim: 10228,
    tenPhim: "hoa thiên cốt p112",
    biDanh: "hoa-thien-cot-p112",
    trailer: "https://youtu.be/0HZ9UO7pLfo",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/hoa-thien-cot_gp01.jpg",
    moTa: "hay ha ha ha",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-04T08:23:40.893",
    danhGia: 1,
    hot: false,
    dangChieu: true,
    sapChieu: false,
  },
  {
    maPhim: 10240,
    tenPhim: "Cat and Dog nè hjhj haha j",
    biDanh: "cat-and-dog-ne-hjhj-haha-j",
    trailer: "https://youtu.be/nW948Va-l10",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog_gp01.jpg",
    moTa: "Đã lâu k gặp, nay tình cờ mèo gặp 2 bạn chó già đi thể dục thế là rủ nhau đi uống cafe",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-02T21:07:34.77",
    danhGia: 10,
    hot: true,
    dangChieu: false,
    sapChieu: true,
  },
  {
    maPhim: 10363,
    tenPhim: "Minionaaaaaaaaaaaaa",
    biDanh: "minionaaaaaaaaaaaaa",
    trailer: "ssssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/minion_gp01.png",
    moTa: "...................................",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-02T21:17:02.343",
    danhGia: 8,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10374,
    tenPhim: "Uncharged",
    biDanh: "uncharged",
    trailer: "https://www.youtube.com/watch?v=eHp3MbsCbMg",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/uncharged_gp01.jpg",
    moTa: "ádfjsaldkfsdf",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-03T00:00:00",
    danhGia: 8,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10377,
    tenPhim: "Cat and Dog12",
    biDanh: "cat-and-dog12",
    trailer: "ádasdasd",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog12_gp01.jpg",
    moTa: "loremroermjeo",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-04T00:00:00",
    danhGia: 2,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10378,
    tenPhim: "Cat and Dog1234",
    biDanh: "cat-and-dog1234",
    trailer: "ádasdasd",
    hinhAnh:
      "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog1234_gp01.jpg",
    moTa: "loremroermjeo",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-05T00:00:00",
    danhGia: 4,
    hot: false,
    dangChieu: true,
    sapChieu: false,
  },
  {
    maPhim: 10379,
    tenPhim: "ád",
    biDanh: "ad",
    trailer: "ádasdasd",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/ad_gp01.jpg",
    moTa: "Đã lâu k gặp, nay tình cờ mèo gặp 2 bạn chó già đi thể dục thế là rủ nhau đi uống cafe",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-02T00:00:00",
    danhGia: 4,
    hot: false,
    dangChieu: true,
    sapChieu: false,
  },
  {
    maPhim: 10380,
    tenPhim: "ád",
    biDanh: "ad",
    trailer: "https://youtu.be/nW948Va-l10",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/ad_gp01.jpg",
    moTa: "Đã lâu k gặp, nay tình cờ mèo gặp 2 bạn chó già đi thể dục thế là rủ nhau đi uống cafe",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-05T00:00:00",
    danhGia: 6,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10381,
    tenPhim: "dấdasdadqwd",
    biDanh: "dadasdadqwd",
    trailer: "https://youtu.be/nW948Va-l10",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog_gp01.jpg",
    moTa: "adasdadsadadwqd",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-03T16:46:13.637",
    danhGia: 5,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10387,
    tenPhim: "Cat and Dog",
    biDanh: "cat-and-dog",
    trailer: "https://youtu.be/nW948Va-l10",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog_gp01.png",
    moTa: "loremroermjeo",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-03T00:00:00",
    danhGia: 6,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
  {
    maPhim: 10388,
    tenPhim: "Loki",
    biDanh: "loki",
    trailer: "https://youtu.be/nW948Va-l10",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/loki_gp01.png",
    moTa: "Loki là một bộ phim truyền hình dài tập Mỹ ra mắt năm 2021, phát độc quyền trên nền tảng trực tuyến Disney+ của đạo diễn Michael Waldron. Phim dựa trên nhân vật cùng tên từ truyện tranh Marvel Comics.",
    maNhom: "GP01",
    ngayKhoiChieu: "2022-10-20T00:00:00",
    danhGia: 10,
    hot: true,
    dangChieu: true,
    sapChieu: true,
  },
];
